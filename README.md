A simple command-line utility to crop and convert raw images to jpg or tiff.
This was used to process raw images for my icicle experiments.

# Building

Simply run `./build.sh`

# Usage

For batch-processing of a directory, use the `batch-convert.sh` script, which
will use [gnu parallel](https://www.gnu.org/software/parallel/) to run 14 jobs
at a time. 

The actual cropping utility can be run as:

```
crop <infile>  -o <outfile> [-d <x,y,w,h>] [-c <configFile>]
```

## Arguments:
```
  -o   Name of the output file
  -d   dimensions of the cropped region (left, top, width, height)
  -c   Name of the config file (not implemented)
  -j   Export as jpeg (currently just tiff with default color profile
```

# Dependencies

- [libraw](https://www.libraw.org/)
- [libjpeg-turbo](https://www.libjpeg-turbo.org/)
