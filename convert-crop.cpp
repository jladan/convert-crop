/* Program to convert a raw image and crop to desired size
 */

// TODO: put in actual error checking

#include <iostream>
#include <string>
#include <vector>

#include "libraw.h"
#include "jpeglib.h"
#include <cstdio>


/* program outline:
 *
 * read filename and crop information
 * initiate libraw object
 * load file
 * check size
 * crop image
 * process
 * save as tiff
 */

enum class FileType {
    tiff,
    jpg
};

void writeJPEG(std::string fname, libraw_processed_image_t & img);

void usage() {
    std::string helptext = "USAGE: crop <infile>  -o <outfile> [-d <x,y,w,h>] [-c <configFile>]\n\n" 
        "Arguments:\n"
        "  -o   Name of the output file\n"
        "  -d   dimensions of the cropped region\n"
        "  -c   Name of the config file (not implemented)\n"
        "  -j   Export as jpeg (currently just tiff with default color profile\n"
        "\nStill to be implemented: configFile, exif data.";
    std::cout << helptext << std::endl;
}

std::vector<int> readRect(const std::string& coords) {
    std::vector<int> rect;

    std::string tmp(coords);
    std::string::size_type sz;

    for (int i = 0; i<3; i++) {
        rect.push_back(std::stoi(tmp, &sz));
        tmp = tmp.substr(sz+1);
        //std::cout << rect[i] << " " << sz << " \"" << tmp << "\"" << std::endl;
    }
    rect.push_back(std::stoi(tmp, &sz));
    //std::cout << rect[3] << " " << sz << " \"" << tmp << "\"" << std::endl;
    return rect;
} 

class ArgParser {
    public:
        ArgParser (int argc, char** argv) {
            for (int i=1; i < argc; i++)
                this->tokens.push_back(std::string(argv[i]));

            for (auto it = this->tokens.begin(); it < this->tokens.end(); it++) {
                if (*it == "-h") {
                    usage();
                    this->quit = 1;
                }
                else if (*it == "-v") {
                    this->verbose = true;
                }
                else if (*it == "-.") {
                    this->dot = true;
                }
                else if (*it == "-j") {
                    this->ftype = FileType::jpg;
                }
                else if ((*it).front() == '-') {
                    // check to make sure there are enough arguments
                    if (it == this->tokens.end() -1 ) {
                        std::cout << "Argument missing value\n";
                        this->quit = 1;
                    }

                    switch ((*it)[1]) {
                        case 'o':  this->outFile = *(++it);
                                   break;
                        case 'c':  this->configFile = *(++it);
                                   break;
                        case 'd':  this->cropRect = readRect(*(++it));
                                   this->crop = true;
                                   break;
                    }
                    //it--;
                    //std::cout << "Option: " << *it << " Value: " << *(++it) << std::endl;

                } else {
                    if (!this->inFile.empty()) 
                        std::cout << "Multiple input files provided. "
                                     "Using the last one." << std::endl;
                    this->inFile = *it;
                    //std::cout << "Filename: " << *it << std::endl;
                }
            }
        }

        std::string inFile, outFile, configFile;
        std::vector<int> cropRect;
        bool verbose = false;
        bool dot = false;
        bool crop = false;
        bool quit = false;
        FileType ftype = FileType::tiff;
    private:
        std::vector<std::string> tokens;
};

std::string ftos(FileType ftype) {
    switch (ftype) {
        case FileType::jpg:  return "jpg";
        case FileType::tiff:
        default:  return "tiff";
    }
}

void printConfig(std::ostream& os, const ArgParser& opt) {
    os << "Config values:\n" ;
    os << "  infile:\t" << opt.inFile << '\n';
    os << "  outfile:\t" << opt.outFile << '\n';
    os << "  ftype: \t" << ftos(opt.ftype) << '\n';
    os << "  config:\t" << opt.configFile << '\n';
    if (opt.crop)
        os << "  x, y:  \t" << opt.cropRect[0] << ", " << opt.cropRect[1] << '\n'
           << "  w, h:  \t" << opt.cropRect[2] << ", " << opt.cropRect[3] << std::endl;
    else
        os << "  crop region undefined" << std::endl;
}

int main(int argc, char* argv[]) {
    int errCode;

    ArgParser args(argc, argv);
    if (args.quit) 
        return 0;

    // Print the config just to figure it out.
    if (args.verbose) 
        printConfig(std::cout, args);
    

    // create image processor
    LibRaw rawProcessor;

    // check that arguments are valid
    if (argc < 2) {
        usage();
        return 1;
    }

    // TODO: put in actual error checking
    rawProcessor.open_file(args.inFile.c_str());
    rawProcessor.unpack();

    // Setting the crop area
    if (args.crop) 
        for (int i=0; i<4; i++)
            rawProcessor.imgdata.params.cropbox[i] = args.cropRect[i];

    libraw_processed_image_t * img;
    rawProcessor.imgdata.params.use_camera_wb = 1;
    if (args.ftype == FileType::tiff) {
        // Setting 16-bit linear tiff output
        rawProcessor.imgdata.params.output_tiff = 1;
        rawProcessor.imgdata.params.output_bps = 16;
        rawProcessor.imgdata.params.gamm[0] = 
            rawProcessor.imgdata.params.gamm[1] =
            rawProcessor.imgdata.params.no_auto_bright = 1;
        rawProcessor.dcraw_process();
        rawProcessor.dcraw_ppm_tiff_writer(args.outFile.c_str());
    }
    else if (args.ftype == FileType::jpg) {
        rawProcessor.imgdata.params.output_tiff = 1;
        rawProcessor.dcraw_process();
        img = rawProcessor.dcraw_make_mem_image(&errCode);
        writeJPEG(args.outFile, *img);
    }

    if (args.dot)
        std::cout << '.' << std::endl;

    // output in xyz
    //rawProcessor.imgdata.params.output_color = 5;

    
    return 0;
}

void writeJPEG(std::string fname, libraw_processed_image_t & img) {
    // This code comes from the libjpeg example
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;

    FILE * outfile;
    JSAMPROW row_pointer[1];

    // Set up error handler and initialize jpeg object
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);

    // Output file has to be c stream
    if ((outfile = fopen(fname.c_str(), "wb")) == NULL) { 
        std::cerr << "can't open " << fname << std::endl;
        exit(1);
    }
    jpeg_stdio_dest(&cinfo, outfile);

    cinfo.image_width = img.width;
    cinfo.image_height = img.height;
    cinfo.input_components = 3;
    cinfo.in_color_space = JCS_RGB;
    int row_stride = img.width * 3;

    jpeg_set_defaults(&cinfo);
    jpeg_set_quality(&cinfo, 90, TRUE);

    jpeg_start_compress(&cinfo, TRUE);

    while (cinfo.next_scanline < cinfo.image_height) {
        row_pointer[0] = & img.data[cinfo.next_scanline * row_stride];
        (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }

    jpeg_finish_compress(&cinfo);
    fclose(outfile);
    jpeg_destroy_compress(&cinfo);
}
