#!/usr/bin/env bash
usage="(basename "$0") <raw-dir> <converted-dir> [jpeg] -- convert all images in <raw-dir> to tiff or jpeg.

where:
    <raw-dir>   is the directory with raw images
    <converted-dir> is the directory to save the converted images
    jpeg        sets the output format to jpeg (default is tiff)
"

# Make the output directory if it doesn't exist
mkdir -p $2

# Check for whether to use jpeg or tiff
# Tiff is default

if [ $# -gt 2 ] && [ $3 = jpeg  ]; then
    echo "Using jpeg"
    ext=jpg
    format_flag='-j'
else
    echo "Using tiff"
    ext=tiff
    format_flag=""
fi

echo "$2/{/.}.$ext" $format_flag 

# use `parallel` to crop all the files
parallel -j 14 crop "{}" -o "$2/{/.}.$ext" $format_flag -d 0,1744,10000,1540 ::: $1/*.nef
